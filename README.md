# README #

This is a simple project for experimenting with the Ink / Unity engines for the PIF project.

### What is this repository for? ###

* This Repo is for testing the capabilities of Unity

### How do I get set up? ###

* Clone this Repo onto your local drive
* Add the files as a Unity Project in the Unity Editor
* Open the Example Scene


### Who do I talk to? ###

* For full access to this Repo, contact me on the IDC HCI Slack (Gilad Ostrin)